<?php

use Api\Infrastructure\Factories\EntityManagerFactory;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
require_once "vendor/autoload.php";

$entityManager = new EntityManagerFactory();
$entity = $entityManager->getEntityManager();

// replace with mechanism to retrieve EntityManager in your app
return ConsoleRunner::createHelperSet($entity);
