# Order Pad Api

## 💻 Pré-requisitos

Antes de começar, verifique se você atendeu aos seguintes requisitos:
* Você instalou a versão mais recente do DOCKER


## 🚀 Instalando Order Pad Api
Para instalar o order pad api, siga estas etapas:

Navegue até o diretório onde estão localizados seus projetos:
```
cd projects
```
Clone o repositório:
```
git clone https://gitlab.com/RomuloCVieira/order-pad-api.git
```
Suba os containers:

```
docker-compose up -d --build
```
Entre no container da nossa api para instalar as dependencias:
```
docker exec -it order-pad-api bash
```

Instalando dependencias:
```
/var/www/html# composer install
```

Criando nosso banco e tabelas:
```
/var/www/html# vendor/bin/doctrine orm:schema-tool:create
```

## 🤝 Colaboradores
Agradecemos às seguintes pessoas que contribuíram para este projeto:

<table>
  <tr>
    <td align="center">
      <a href="https://gitlab.com/RomuloCVieira/">
        <img src="https://gitlab.com/uploads/-/system/user/avatar/12269576/avatar.png?width=400" width="100px;" alt="Foto do Iuri Silva no GitHub"/><br>
        <sub>
          <b>Romulo C Vieira</b>
        </sub>
      </a>
    </td>
  </tr>
</table>

## 📝 Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE.md) para mais detalhes.

[⬆ Voltar ao topo](#order-pad-api)<br>
