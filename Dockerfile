FROM php:8.0-apache

RUN apt update

RUN a2enmod rewrite

RUN apt install git \
    unzip \
    zip \
    tar -y

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"

#Driver para conexão PDO mysql 
RUN docker-php-ext-install pdo_mysql

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN pecl install xdebug && docker-php-ext-enable xdebug

COPY config/xdebug/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/
