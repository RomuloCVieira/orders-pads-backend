<?php

declare(strict_types=1);

namespace Tests\Application\OrderPad;

use Api\Application\OrderPad\OpenOrderPadAction;
use Api\Domain\OrderPad\OrderPad;
use Api\Infrastructure\Helpers\Hydrator;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadDTO;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadItemDTO;
use Api\Infrastructure\Repositories\OrderPad\InMemoryOrderPadRepository;
use PHPUnit\Framework\TestCase;

class OrderPadTest extends TestCase
{
    public function testOpenOrderPadIsSuccess()
    {
        $inMemoryOrderPadRepository = new InMemoryOrderPadRepository();
        $openOrderpad = new OpenOrderPadAction($inMemoryOrderPadRepository);

        /**@var $orderPadDTO OrderPadDTO*/
        $orderPadDTO = Hydrator::hydrate(
            ['description' => 'Joao Eduardo'],
            OrderPadDTO::class,
            OrderPadItemDTO::class
        );

        $orderPad  = $openOrderpad->action($orderPadDTO);

        $this->assertEquals(3, $orderPad->getIdt());
        $this->assertInstanceOf(OrderPad::class, $orderPad);
    }
}
