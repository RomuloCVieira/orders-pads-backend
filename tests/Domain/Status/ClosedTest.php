<?php

declare(strict_types=1);

namespace Tests\Domain\Status;

use Api\Domain\Status\Closed;
use Api\Domain\Status\Downloaded;
use Api\Domain\Status\Open;
use DomainException;
use PHPUnit\Framework\TestCase;

class ClosedTest extends TestCase
{
    public function dataProvider(): array
    {
        return [
            'status fechado'  => [new Closed()]
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testOpenOrderPadIsSuccess(Closed $status)
    {
        $status = $status->openOrderPad();

        $this->assertInstanceOf(Open::class, $status);
        $this->assertEquals(Open::OPEN, $status->getId());
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCloseOrderPadShouldThrowExeption(Closed $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel fechar a comanda');

        $status->closeOrderPad();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testDownloadOrderPadIsSuccess(Closed $status)
    {
        $status = $status->downloadOrderPad();

        $this->assertInstanceOf(Downloaded::class, $status);
        $this->assertEquals(Downloaded::DOWNLOADED, $status->getId());
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCancelOrderPadShouldThrowExeption(Closed $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel cancelar a comanda');

        $status->cancelOrderPad();
    }
}
