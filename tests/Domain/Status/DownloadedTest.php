<?php

declare(strict_types=1);

namespace Tests\Domain\Status;

use Api\Domain\Status\Downloaded;
use DomainException;
use PHPUnit\Framework\TestCase;

class DownloadedTest extends TestCase
{
    public function dataProvider(): array
    {
        return [
            'status baixado' => [new Downloaded()]
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testOpenOrderPadShouldThrowExeption(Downloaded $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel abrir comanda');

        $status->openOrderPad();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCloseOrderPadShouldThrowExeption(Downloaded $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel fechar a comanda');

        $status->closeOrderPad();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testDownloadOrderPadShouldThrowExeption(Downloaded $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel baixar a comanda');

        $status->downloadOrderPad();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCancelOrderPadShouldThrowExeption(Downloaded $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel cancelar a comanda');

        $status->cancelOrderPad();
    }
}
