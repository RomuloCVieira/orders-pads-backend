<?php

declare(strict_types=1);

namespace Tests\Domain\Status;

use Api\Domain\Status\Canceled;
use DomainException;
use PHPUnit\Framework\TestCase;

class CanceledTest extends TestCase
{
    public function dataProvider(): array
    {
        return [
            'status cacelado' => [new Canceled()]
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testOpenOrderPadShouldThrowExeption(Canceled $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel abrir comanda');

        $status->openOrderPad();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCloseOrderPadShouldThrowExeption(Canceled $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel fechar a comanda');

        $status->closeOrderPad();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testDownloadOrderPadShouldThrowExeption(Canceled $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel baixar a comanda');

        $status->downloadOrderPad();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCancelOrderPadShouldThrowExeption(Canceled $status)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não é possivel cancelar a comanda');

        $status->cancelOrderPad();
    }
}
