<?php

declare(strict_types=1);

namespace Tests\Domain\Status;

use Api\Domain\Status\Canceled;
use Api\Domain\Status\Closed;
use Api\Domain\Status\Downloaded;
use Api\Domain\Status\Open;
use Api\Domain\Status\Status;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Api\Domain\Status\Status
 */
class StatusTest extends TestCase
{
    /**
     * @covers ::getStatus
     */
    public function testGetStatus()
    {
        $open = Status::getStatus(Open::OPEN);
        $closed = Status::getStatus(Closed::CLOSED);
        $canceled = Status::getStatus(Canceled::CANCELED);
        $downloaded = Status::getStatus(Downloaded::DOWNLOADED);

        $this->assertInstanceOf(Open::class, $open);
        $this->assertInstanceOf(Closed::class, $closed);
        $this->assertInstanceOf(Canceled::class, $canceled);
        $this->assertInstanceOf(Downloaded::class, $downloaded);
    }
}
