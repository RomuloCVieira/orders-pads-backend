<?php

declare(strict_types=1);

namespace Tests\Domain\OrderPad;

use Api\Domain\OrderPadItem\OrderPadItem;
use Api\Domain\Product\Product;
use PHPUnit\Framework\TestCase;

class OrderPadItemTest extends TestCase
{
    public function testCreditItem()
    {
        $item = OrderPadItem::build([
            'product' => Product::build([
                'idt' => 2,
                'description' => 'Coca Cola',
                'amount' => 10.00
            ]),
            'quantity' => 2,
            'amount' => 20.00,
            'idtOrderPad' => 10,
            'idt' => 1
        ]);

        $item->crediteItem();

        $this->assertEquals(30.00, $item->amount);
        $this->assertEquals(3, $item->quantity);
    }

    public function testDebitItem()
    {
        $item = OrderPadItem::build([
            'product' => Product::build([
                'idt' => 2,
                'description' => 'Coca Cola',
                'amount' => 10.00
            ]),
            'quantity' => 2,
            'amount' => 20.00,
            'idtOrderPad' => 10,
            'idt' => 1
        ]);


        $item->debitItem();

        $this->assertEquals(10.00, $item->amount);
        $this->assertEquals(1, $item->quantity);
    }
}
