<?php

declare(strict_types=1);

namespace Tests\Domain\OrderPad;

use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPad\OrderPadList;
use Api\Domain\Status\Open;
use ArrayIterator;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Api\Domain\OrderPad\OrderPadList
 */
class OrderPadListTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::addOrderPad
     * @covers ::getIterator
     */
    public function testOrderPadIterator()
    {
        $orderPadList = new OrderPadList();

        $orderPad = OrderPad::build([
            'status' => new Open(),
            'description' => 'Romulo Vieira'
        ]);

        $orderPadList->addOrderPad($orderPad);

        $iterator = $orderPadList->getIterator();

        $this->assertInstanceOf(ArrayIterator::class, $iterator);
        $this->assertEquals([$orderPad], $iterator->getArrayCopy());
    }
}
