<?php

declare(strict_types=1);

namespace Tests\Domain\OrderPad;

use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPadItem\OrderPadItem;
use Api\Domain\Product\Product;
use Api\Domain\Status\Canceled;
use Api\Domain\Status\Closed;
use Api\Domain\Status\Downloaded;
use Api\Domain\Status\Open;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadItemDTO;
use DomainException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Api\Domain\OrderPad\OrderPad
 */
class OrderPadTest extends TestCase
{
    private function firstOrderPadMock(): OrderPad
    {
        $orderPad = OrderPad::build([
            'status' => new Open(),
            'description' => 'Romulo Vieira'
        ]);
        $orderPad->addItem(new OrderPadItem(
            product: Product::build([
                'idt' => 2,
                'description' => 'Coca Cola',
                'amount' => 10.00
            ]),
            quantity: 2,
            amount: 20.00,
            idtOrderPad: 1,
            idt: 1
        ));
        $orderPad->addItem(new OrderPadItem(
            product: Product::build([
                'idt' => 1,
                'description' => 'Batata Frita',
                'amount' => 30.00
            ]),
            quantity: 1,
            amount: 30.00,
            idtOrderPad: 1,
            idt: 1
        ));

        return $orderPad;
    }

    public function dataProvider(): array
    {
        // arrange — Um cenário a ser testado (um “dado”).
        $orderPad = OrderPad::build([
            'status' => new Open(),
            'description' => 'Romulo Vieira'
        ]);
        $orderPad->addItem(new OrderPadItem(
            product: Product::build([
                'idt' => 2,
                'description' => 'Coca Cola',
                'amount' => 10.00
            ]),
            quantity: 2,
            amount: 20.00,
            idtOrderPad: 1,
            idt: 1
        ));
        $orderPad->addItem(new OrderPadItem(
            product: Product::build([
                'idt' => 1,
                'description' => 'Batata Frita',
                'amount' => 30.00
            ]),
            quantity: 1,
            amount: 30.00,
            idtOrderPad: 1,
            idt: 1
        ));

        return [
             'first-test' => [$this->firstOrderPadMock(), OrderPad::CREDIT, 80.00],
             'second-test' => [$orderPad, OrderPad::DEBIT, 20.00]
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCalculateTotalAmountWithItems(OrderPad $orderPad)
    {
        $amount = $orderPad->calculateTotalAmount();

        $this->assertEquals(50.00, $amount);
    }

    /**
     * @dataProvider dataProvider
     * @covers ::creditOrDebitItems
     */
    public function testCreditOrDebitIsSucess(OrderPad $orderPad, string $method, float $expect)
    {
        $orderPadItems = [
            OrderPadItem::build([
                'product' => Product::build([
                    'idt' => 1,
                    'description' => 'Batata Frita',
                    'amount' => 30.00
                ]),
                'quantity' => 1,
                'amount' => 30.00,
                'idtOrderPad' => 1
            ])
        ];

        $orderPad->creditOrDebitItems($orderPadItems, $method);

        $this->assertEquals($expect, $orderPad->getAmount());
    }

    public function testCalculateTotalAmountWithoutItems()
    {
        $orderPad = OrderPad::build([
            'status' => new Open(),
            'description' => 'Romulo Vieira'
        ]);

        $amount = $orderPad->calculateTotalAmount();

        $this->assertEquals(00.00, $amount);
    }

    /**
     * @dataProvider dataProvider
     * @covers ::addItem
     * @covers ::addItems
     * @covers ::findProduct
     */
    public function testAddItems(OrderPad $orderPad)
    {
        $items = [
            new OrderPadItemDTO(
                idtproduct: 1,
                product: 'batata frita',
                quantity: 2,
                amount: 60.00,
                idcomanda: 1
            ),
            new OrderPadItemDTO(
                idtproduct: 3,
                product: 'Peixe Frito',
                quantity: 2,
                amount: 70.00,
                idcomanda: 1
            ),
        ];

        $orderPad->addItems($items);

        $this->assertCount(3, $orderPad->getItems());
    }

    /**
     * @dataProvider dataProvider
     */
    public function testAdicioneItemComStatusFechadoDeveLancarExcecao(OrderPad $comanda)
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Não pode ser adionado item, comanda FECHADA');

        $comanda->updateStatus(Closed::CLOSED);
        $comanda->addItem(new OrderPadItem(
            product: Product::build([
                'idt' => 1,
                'description' => 'Batata Frita',
                'amount' => 30.00
            ]),
            quantity: 2,
            amount: 60.00,
            idtOrderPad: 1,
            idt: 1
        ));
    }

    /**
     * @dataProvider dataProvider
     */
    public function testUpdateStatusToOpenIsSuccess(OrderPad $orderPad)
    {
        $orderPad->updateStatus(Closed::CLOSED);
        $orderPad->updateStatus(Open::OPEN);

        self::assertInstanceOf(Open::class, $orderPad->getStatus());
    }

    /**
     * @dataProvider dataProvider
     */
    public function testUpdateStatusToDownloadedIsSuccess(OrderPad $orderPad)
    {
        $orderPad->updateStatus(Closed::CLOSED);
        $orderPad->updateStatus(Downloaded::DOWNLOADED);

        self::assertInstanceOf(Downloaded::class, $orderPad->getStatus());
    }

    /**
     * @dataProvider dataProvider
     */
    public function testUpdateStatusToCanceledIsSuccess(OrderPad $orderPad)
    {
        $orderPad->updateStatus(Canceled::CANCELED);

        self::assertInstanceOf(Canceled::class, $orderPad->getStatus());
    }

    public function testinvalidDescriptionShouldThrowException()
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Descrição invalida para OrderPad');

        OrderPad::build([
            'status' => new Open(),
            'description' => 'Romulo'
        ]);
    }

    public function testOrderPadIsEmptyShouldThrowException()
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('OrderPad não pode ser fechada sem itens');

        $orderPad = OrderPad::build([
            'status' => new Open(),
            'description' => 'Romulo Vieira'
        ]);

        $orderPad->updateStatus(Closed::CLOSED);
    }
}
