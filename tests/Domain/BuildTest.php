<?php

declare(strict_types=1);

namespace Tests\Domain;

use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPadItem\OrderPadItem;
use Api\Domain\Product\Product;
use Api\Domain\Status\Open;
use PHPUnit\Framework\TestCase;

class BuildTest extends TestCase
{
    public function testbuildInstanceIsSuccess(): void
    {
        $orderPad = OrderPad::build([
            'status' => new Open(),
            'description' => 'Romulo Vieira'
        ]);

        $orderPadItem = OrderPadItem::build([
            'product' => Product::build([
                'idt' => 2,
                'description' => 'Coca Cola',
                'amount' => 10.00
            ]),
            'quantity' => 2,
            'amount' => 20.00,
            'idtOrderPad' => 10,
            'idt' => 1
        ]);

        $this->assertInstanceOf(OrderPad::class, $orderPad);
        $this->assertInstanceOf(OrderPadItem::class, $orderPadItem);
    }
}
