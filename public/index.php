<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use Api\Application\OrderPad\AddItemToOrderPadAction;
use Api\Application\OrderPad\DebitItemsAction;
use Api\Application\OrderPad\FindByIdAction;
use Api\Application\OrderPad\FindOrderPadAction;
use Api\Application\OrderPad\OpenOrderPadAction;
use Api\Application\OrderPad\ToCreditItemsAction;
use Api\Application\OrderPad\UpdateStatusAction;
use Api\Infrastructure\Factories\ContainerFactory;
use Api\Infrastructure\Http\Middleware\JsonBodyParserMiddleware;
use Api\Infrastructure\Http\Middleware\JsonSchemaValidator;
use Slim\Factory\AppFactory;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

$container = (new ContainerFactory())();

$app = AppFactory::createFromContainer($container);

$app->add(new JsonSchemaValidator());

$app->post('/orderpad/open', OpenOrderPadAction::class)->add(new JsonBodyParserMiddleware());
$app->get('/orderpad/findall', FindOrderPadAction::class)->add(new JsonBodyParserMiddleware());
$app->get('/orderpad/show/{id}', FindByIdAction::class)->add(new JsonBodyParserMiddleware());
$app->post('/orderpad/additem', AddItemToOrderPadAction::class)->add(new JsonBodyParserMiddleware());
$app->put('/orderpad/updatestatus', UpdateStatusAction::class)->add(new JsonBodyParserMiddleware());
$app->put('/orderpad/tocredititems', ToCreditItemsAction::class)->add(new JsonBodyParserMiddleware());
$app->put('/orderpad/debititems', DebitItemsAction::class)->add(new JsonBodyParserMiddleware());

$app->run();
