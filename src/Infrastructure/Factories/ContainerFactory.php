<?php

declare(strict_types=1);

namespace Api\Infrastructure\Factories;

use Api\Application\OrderPad\AddItemToOrderPadAction;
use Api\Application\OrderPad\DebitItemsAction;
use Api\Application\OrderPad\FindByIdAction;
use Api\Application\OrderPad\OpenOrderPadAction;
use Api\Application\OrderPad\FindOrderPadAction;
use Api\Application\OrderPad\ToCreditItemsAction;
use Api\Application\OrderPad\UpdateStatusAction;
use Api\Infrastructure\Http\Controllers\OrderPad\OrderPadController;
use Api\Infrastructure\Repositories\OrderPad\OrderPadItemRepository;
use Api\Infrastructure\Repositories\OrderPad\OrderPadRepository;
use DI\Container;
use Psr\Container\ContainerInterface;

class ContainerFactory
{
    private Container $container;

    public function __construct()
    {
        $this->container = new Container();
    }

    public function __invoke(): Container
    {
        $this->setDefinitions();
        return $this->container;
    }

    private function setDefinitions(): void
    {
        $this->setRepositories();

        $this->setActions();

        $this->setControllers();
    }


    private function setRepositories(): void
    {
        $this->container->set(OrderPadItemRepository::class, function () {
            return new OrderPadItemRepository(
                EntityManagerFactory::getEntityManager()
            );
        });

        $this->container->set(OrderPadRepository::class, function (ContainerInterface $container) {
            return new OrderPadRepository(
                EntityManagerFactory::getEntityManager(),
                $container->get(OrderPadItemRepository::class)
            );
        });
    }


    private function setActions(): void
    {
        $this->container->set(OpenOrderPadAction::class, function (ContainerInterface $container) {
            return new OrderPadController(
                new OpenOrderPadAction($container->get(OrderPadRepository::class))
            );
        });

        $this->container->set(FindOrderPadAction::class, function (ContainerInterface $container) {
            return new OrderPadController(
                new FindOrderPadAction($container->get(OrderPadRepository::class))
            );
        });

        $this->container->set(FindByIdAction::class, function (ContainerInterface $container) {
            return new OrderPadController(
                new FindByIdAction($container->get(OrderPadRepository::class))
            );
        });

        $this->container->set(AddItemToOrderPadAction::class, function (ContainerInterface $container) {
            return new OrderPadController(
                new AddItemToOrderPadAction($container->get(OrderPadRepository::class))
            );
        });

        $this->container->set(UpdateStatusAction::class, function (ContainerInterface $container) {
            return new OrderPadController(
                new UpdateStatusAction($container->get(OrderPadRepository::class))
            );
        });

        $this->container->set(ToCreditItemsAction::class, function (ContainerInterface $container) {
            return new OrderPadController(
                new ToCreditItemsAction($container->get(OrderPadRepository::class))
            );
        });

        $this->container->set(DebitItemsAction::class, function (ContainerInterface $container) {
            return new OrderPadController(
                new DebitItemsAction($container->get(OrderPadRepository::class))
            );
        });
    }

    private function setControllers(): void
    {
        $this->container->set(OrderPadController::class, function (ContainerInterface $container) {
            return new OrderPadController($container->get(ToCreditItemsAction::class));
        });
    }
}
