<?php

declare(strict_types=1);

namespace Api\Infrastructure\Factories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\ORMSetup;

class EntityManagerFactory
{
    /**
     * @return EntityManager
     * @throws ORMException
     */
    public static function getEntityManager(): EntityManager
    {
        $config = ORMSetup::createAnnotationMetadataConfiguration(
            [(__DIR__."/../Entities")],
            true
        );

        $connectionParams = [
            'dbname' => 'apicomanda',
            'user' => 'root',
            'password' => 'root',
            'host' => 'db',
            'driver' => 'pdo_mysql',
        ];

        return EntityManager::create($connectionParams, $config);
    }
}
