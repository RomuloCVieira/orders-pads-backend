<?php

declare(strict_types=1);

namespace Api\Infrastructure\Helpers;

use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadDTO;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadItemDTO;
use ReflectionClass;

class Hydrator
{
    private static array $reflectionClassMap;

    private static function getReflectionClass(string $className): ReflectionClass
    {
        if (!isset(self::$reflectionClassMap[$className])) {
            self::$reflectionClassMap[$className] = new ReflectionClass($className);
        }

        return self::$reflectionClassMap[$className];
    }

    public static function hydrate(array|null|object $data, string ...$entities): OrderPadDTO|OrderPadItemDTO
    {
        [$entity] = $entities;
        $reflection = self::getReflectionClass($entity);
        $orderPad = $reflection->newInstanceWithoutConstructor();

        if (empty($data)) {
            return $orderPad;
        }

        foreach ($data as $name => $value) {
            $attribute = $reflection->getProperty($name);

            if (is_array($value)) {
                $items = [];
                foreach ($value as $item) {
                    $items[] = self::hydrate($item, $entities[1]);
                }

                $value = empty($items) ? $value : $items;
            }

            if ($attribute->isPrivate() || $attribute->isProtected()) {
                $attribute->setAccessible(true);
            }

            $attribute->setValue($orderPad, $value);
        }

        return $orderPad;
    }

    public static function extract(object $object, array $fields): array
    {
        $reflection = self::getReflectionClass(get_class($object));
        $result = [];

        foreach ($fields as $name) {
            $property = $reflection->getProperty($name);
            if ($property->isPrivate() || $property->isProtected()) {
                $property->setAccessible(true);
            }

            $result[$property->getName()] = $property->getValue($object);
        }

        return $result;
    }
}
