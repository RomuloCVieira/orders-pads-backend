<?php

namespace Api\Infrastructure\Repositories\OrderPad;

use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPad\OrderPadList;
use Api\Domain\RepositoryInterface;

class InMemoryOrderPadRepository implements RepositoryInterface
{
    private array $orderpads;

    public function __construct(array $orderpads = null)
    {
        $this->orderpads = ($orderpads ?? $this->dataProvider());
    }

    private function dataProvider(): array
    {
        return [
            [
                'idt' => 1,
                'idstatus' => 1,
                'descricao' => 'Romulo Vieira',
                'valorTotal' => 0,
            ],
            [
                'idt' => 2,
                'idstatus' => 1,
                'descricao' => 'joas Vieira',
                'valorTotal' => 0,
            ]
        ];
    }

    public function save(OrderPad $orderPad): OrderPad
    {
        $lastId = 3;

        $orderPadArray = [
            'idt' => $lastId,
            'idstatus' => $orderPad->getStatus()->getId(),
            'descricao' => $orderPad->getDescription(),
            'valorTotal' => $orderPad->getAmount()
        ];

        array_push($this->orderpads, $orderPadArray);

        $orderPad->setIdt($lastId);

        return $orderPad;
    }

    public function findAll(): OrderPadList
    {
        $orderPadList = new OrderPadList();
        foreach ($this->orderpads as $orderPad) {
            $orderPadList->addOrderPad($this->findById($orderPad['idt']));
        }

        return $orderPadList;
    }

    public function findById(int $idOrderPad): OrderPad
    {
        // TODO: Implement findById() method.
    }

    public function update(OrderPad|array $orderPad): OrderPad
    {
        // TODO: Implement update() method.
    }
}
