<?php

declare(strict_types=1);

namespace Api\Infrastructure\Repositories\OrderPad;

use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPad\OrderPadList;
use Api\Domain\OrderPadItem\OrderPadItem;
use Api\Domain\Product\Product;
use Api\Domain\RepositoryInterface;
use Api\Infrastructure\Entities\OrderPad\OrderPadItemEntity;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;

class OrderPadItemRepository implements RepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param OrderPad $orderPad
     * @return OrderPad
     * @throws Exception
     */
    public function save(OrderPad $orderPad): OrderPad
    {
        if ($orderPad->itemsIsNotEmpty()) {
            foreach ($orderPad->getItems() as $item) {
                if (!$item->idt) {
                    $this->entityManager->getConnection()->insert(
                        table: 'order_pad_item',
                        data: [
                            'idt_order_pad' => $orderPad->getIdt(),
                            'idt_product' => $item->getProduct()->getIdt(),
                            'quantity' => $item->quantity,
                            'amount' => $item->amount
                        ],
                        types: [
                            'integer', 'string', 'integer', 'float'
                        ]
                    );

                    $item->id = (int) $this->entityManager->getConnection()->lastInsertId('itemcomanda');
                }
            }
        }

        return $orderPad;
    }

    public function update(OrderPad|array $orderPad): OrderPad|array
    {
        if (!empty($orderPad)) {
            foreach ($orderPad as $item) {
                /**@var $item OrderPadItem*/
                $queryBuilder = $this->entityManager->createQueryBuilder()
                    ->update(OrderPadItemEntity::class, 'i')
                    ->set('i.amount', ':amount')
                    ->set('i.quantity', ':quantity')
                    ->where('i.idt = :idt')
                    ->setParameter('amount', $item->amount)
                    ->setParameter('quantity', $item->quantity)
                    ->setParameter('idt', $item->idt);

                $queryBuilder->getQuery()->getResult();
            }
        }

        return $orderPad;
    }

    public function findAll(): OrderPadList
    {
        return new OrderPadList();
    }

    public function findById(int $idOrderPad): array
    {
        $queryBuild = $this->entityManager->createQueryBuilder()
            ->select([
                'i.idt',
                'c.idt as idt_order_pad',
                'p.idt as idt_product',
                'p.description',
                'p.amount as product_amount',
                'i.quantity',
                'i.amount'
            ])->from(OrderPadItemEntity::class, 'i')
            ->join('i.idt_order_pad', 'c')
            ->join('i.idt_product', 'p')
            ->where('c.idt = :idtorderpad')
            ->setParameter('idtorderpad', $idOrderPad);
        $items = $queryBuild->getQuery()->getResult();

        if (empty($items)) {
            return $items;
        }

        return $this->buildOrderPadItems($items);
    }

    /**
     * @param array $items
     * @return array
     */
    private function buildOrderPadItems(array $items): array
    {
        return array_map(function ($item) {
            return OrderPadItem::build([
                'idt' => $item['idt'],
                'idtOrderPad' => $item['idt_order_pad'],
                'product' => Product::build([
                    'idt' => $item['idt_product'],
                    'description' => $item['description'],
                    'amount' => $item['product_amount']
                ]),
                'quantity' => $item['quantity'],
                'amount' => $item['amount']
            ]);
        }, $items);
    }
}
