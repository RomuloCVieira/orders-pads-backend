<?php

declare(strict_types=1);

namespace Api\Infrastructure\Repositories\OrderPad;

use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPad\OrderPadList;
use Api\Domain\RepositoryInterface;
use Api\Domain\Status\Open;
use Api\Domain\Status\Status;
use Api\Infrastructure\Entities\OrderPad\OrderPadEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class OrderPadRepository implements RepositoryInterface
{

    private EntityManagerInterface $entityManager;
    private RepositoryInterface $orderPadItemRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        RepositoryInterface $orderPadItemRepository
    ) {
        $this->entityManager = $entityManager;
        $this->orderPadItemRepository = $orderPadItemRepository;
    }

    public function save(OrderPad $orderPad): OrderPad
    {
        if (!$orderPad->getIdt()) {
            $this->entityManager->getConnection()->insert(
                table: 'order_pad',
                data: [
                    'description' => $orderPad->getDescription(),
                    'idtstatus' => $orderPad->getStatus()->getId()
                ],
                types: [
                    'string', 'integer'
                ]
            );

            $orderPad->setIdt((int) $this->entityManager->getConnection()->lastInsertId('comanda'));
        }

        return $this->orderPadItemRepository->save($orderPad);
    }

    /**
     * @param OrderPad $orderPad
     * @return OrderPad
     */
    public function update(OrderPad|array $orderPad): OrderPad|array
    {
        $queryBuilder = $this->entityManager->createQueryBuilder()
            ->update(OrderPadEntity::class, 'c')
            ->set('c.idtstatus', ':idtstatus')
            ->where('c.idt = :idorderPad')
            ->setParameter('idorderPad', $orderPad->getIdt())
            ->setParameter('idtstatus', $orderPad->getStatus()->getId());

        $queryBuilder->getQuery()->getResult();

        $this->orderPadItemRepository->update($orderPad->getItems());

        return $orderPad;
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findAll(): OrderPadList
    {
        $queryBuild = $this->entityManager->createQueryBuilder()
            ->select([
                'c.idt',
                'c.idtstatus',
                'c.description'
            ])->from(OrderPadEntity::class, 'c')
            ->where('c.idtstatus = :idtstatus')
            ->setParameter('idtstatus', Open::OPEN);

        $ordersPads = $queryBuild->getQuery()->getResult();

        $orderPadList = new OrderPadList();
        foreach ($ordersPads as $orderPad) {
            $orderPadList->addOrderPad($this->findById($orderPad['idt']));
        }

        return $orderPadList;
    }

    /**
     * @param int $idOrderPad
     * @return OrderPad
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findById(int $idOrderPad): OrderPad
    {
        $queryBuild = $this->entityManager->createQueryBuilder()
            ->select([
                'c.idt',
                'c.idtstatus',
                'c.description'
            ])->from(OrderPadEntity::class, 'c')
            ->where('c.idt = :idtorderpad')
            ->setParameter('idtorderpad', $idOrderPad);

        $orderPad = $queryBuild->getQuery()->getSingleResult();

        return $this->buildOrderPad($orderPad);
    }

    private function buildOrderPad(array $orderPad): OrderPad
    {
        $items = $this->orderPadItemRepository->findById($orderPad['idt']);

        return OrderPad::build([
            'idt' => $orderPad['idt'],
            'description' => $orderPad['description'],
            'status' => Status::getStatus($orderPad['idtstatus']),
            'items' => $items
        ]);
    }
}
