<?php

declare(strict_types=1);

namespace Api\Infrastructure\Presenters;

use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPad\OrderPadList;

class Presenter
{
    public static function gift(OrderPadList|OrderPad $orderPad): string
    {
        if ($orderPad instanceof OrderPadList) {
            return self::arrayToJson($orderPad);
        }

        return self::json($orderPad);
    }

    private static function arrayToJson(OrderPadList $arrayOrderPads): string
    {
        $arrayPresenter = array_map(function ($orderPad) {
            $items = [];

            /**@var $orderPad OrderPad*/
            if ($orderPad->itemsIsNotEmpty()) {
                $items = array_map(function ($item) {
                    return [
                        'idtproduct' => $item->getProduct()->getIdt(),
                        'product' => $item->getProduct()->getDescription(),
                        'amount' => $item->amount,
                        'quantity' => $item->quantity
                    ];
                }, $orderPad->getItems());
            }

            return [
                'idorderpad' => $orderPad->getIdt(),
                'description' => $orderPad->getDescription(),
                'items' => $items,
                'amount' => $orderPad->getAmount(),
                'status' => $orderPad->getStatus()->getId()
            ];
        }, $arrayOrderPads->getIterator()->getArrayCopy());

        return json_encode($arrayPresenter);
    }

    private static function json(OrderPad $orderPad): string
    {
        $items = array_map(function ($item) {
             return [
                 'idtproduct' => $item->getProduct()->getIdt(),
                 'product' => $item->getProduct()->getDescription(),
                 'amount' => $item->amount,
                 'quantity' => $item->quantity
             ];
        }, $orderPad->getItems());

        return json_encode([
            'idorderpad' => $orderPad->getIdt(),
            'description' => $orderPad->getDescription(),
            'items' => $items,
            'amount' => $orderPad->getAmount(),
            'status' => $orderPad->getStatus()->getId()
        ]);
    }
}
