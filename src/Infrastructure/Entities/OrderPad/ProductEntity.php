<?php

declare(strict_types=1);

namespace Api\Infrastructure\Entities\OrderPad;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class ProductEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $idt;

    /**
     * @ORM\Column(type="string")
     */
    public string $description;

    /**
     * @ORM\Column(type="float")
     */
    public float $amount;
}
