<?php

namespace Api\Infrastructure\Entities\OrderPad;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="order_pad")
 */
class OrderPadEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public int $idt;

    /**
     * @ORM\Column(type="string")
     */
    public string $description;


    /**
     * @ORM\Column(type="integer")
     */
    public int $idtstatus;

    /**
     * @param $idt
     * @param $description
     * @param $idtstatus
     */
    public function __construct(int $idt, string $description, int $idtstatus)
    {
        $this->idt = $idt;
        $this->description = $description;
        $this->idtstatus = $idtstatus;
    }
}
