<?php

namespace Api\Infrastructure\Entities\OrderPad;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity
 * @ORM\Table(name="order_pad_item")
 */
class OrderPadItemEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $idt;

    /**
     * @ManyToOne(targetEntity="OrderPadEntity")
     * @JoinColumn(name="idt_order_pad", referencedColumnName="idt")
     */
    private int $idt_order_pad;

    /**
     * @ManyToOne(targetEntity="ProductEntity")
     * @JoinColumn(name="idt_product", referencedColumnName="idt")
     */
    private int $idt_product;

    /**
     * @ORM\Column(type="integer")
     */
    private int $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private float $amount;

    /**
     * @param int $product
     * @param int $quantity
     * @param float $amount
     */
    public function __construct(int $product, int $quantity, float $amount)
    {
        $this->idt_product = $product;
        $this->quantity = $quantity;
        $this->amount = $amount;
    }

    public function getIdt(): int
    {
        return $this->idt;
    }
}
