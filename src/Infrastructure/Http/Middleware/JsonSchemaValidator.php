<?php

declare(strict_types=1);

namespace Api\Infrastructure\Http\Middleware;

use League\OpenAPIValidation\PSR7\Exception\Validation\InvalidBody;
use League\OpenAPIValidation\PSR7\ValidatorBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Response;

class JsonSchemaValidator implements MiddlewareInterface
{
    private const ORDER_PAD_JSON = __DIR__.'/../../Schema/OrderPad/orderpad.json';

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $validator = (new ValidatorBuilder())->fromJsonFile(self::ORDER_PAD_JSON)->getRequestValidator();

        try {
            $validator->validate($request);
        } catch (InvalidBody $invalidBody) {
            $args = $invalidBody->getPrevious()->getTrace()[0]['args'];
            $keys = ['code', 'value', 'message'];

            if (count($args) === 3) {
                $errors = array_combine($keys, $args);
            }

            $response = new Response();
            $response->getBody()->write(json_encode([
                    "message" => "Request validation failed",
                    "errors" => [
                        ($errors ?? $invalidBody->getPrevious()->getMessage())
                    ]
                ]
            ));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(400);
        }

        return $handler->handle($request);
    }
}
