<?php

declare(strict_types=1);

namespace Api\Infrastructure\Http\Controllers\OrderPad;

use Api\Application\OrderPad\AddItemToOrderPadAction;
use Api\Application\OrderPad\OpenOrderPadAction;
use Api\Application\UseCaseInterface;
use Api\Infrastructure\Helpers\Hydrator;
use Api\Infrastructure\Http\Controllers\ControllerInterface;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadDTO;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadItemDTO;
use Api\Infrastructure\Presenters\Presenter;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class OrderPadController implements ControllerInterface
{
    private const HTTP_CODE_CREATE = 201;
    private const HTTP_CODE_OK = 200;

    private UseCaseInterface $useCaseInterface;

    public function __construct(UseCaseInterface $useCaseInterface)
    {
        $this->useCaseInterface = $useCaseInterface;
    }

    public function __invoke(Request $request, Response $response, array $args): ResponseInterface
    {
        $payload = (array) $request->getParsedBody();
        $payload = array_merge($payload, $args);

        /**@var $orderPadDTO OrderPadDTO */
        $orderPadDTO = Hydrator::hydrate($payload, OrderPadDTO::class, OrderPadItemDTO::class);

        $orderPad = $this->useCaseInterface->action($orderPadDTO);

        $presenter = Presenter::gift($orderPad);

        $response->getBody()->write($presenter);
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($this->getHttpCode());
    }

    /** @noinspection PhpSwitchCanBeReplacedWithMatchExpressionInspection */
    private function getHttpCode(): int
    {
        switch ($this->useCaseInterface) {
            case $this->useCaseInterface instanceof AddItemToOrderPadAction:
            case $this->useCaseInterface instanceof OpenOrderPadAction:
                return self::HTTP_CODE_CREATE;
            default:
                return self::HTTP_CODE_OK;
        }
    }
}
