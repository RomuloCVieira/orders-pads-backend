<?php

declare(strict_types=1);

namespace Api\Infrastructure\Http\Controllers\OrderPad\DTOS;

class OrderPadItemDTO
{
    public int $idtproduct;
    public string $product;
    public int $quantity = 0;
    public float $amount = 0.00;
    public ?int $idcomanda = null;

    public function __construct(
        int $idtproduct,
        string $product,
        int $quantity,
        float $amount,
        int $idcomanda
    ) {
        $this->idtproduct = $idtproduct;
        $this->product = $product;
        $this->quantity = $quantity;
        $this->amount = $amount;
        $this->idcomanda = $idcomanda;
    }
}
