<?php

declare(strict_types=1);

namespace Api\Infrastructure\Http\Controllers\OrderPad\DTOS;

class OrderPadDTO
{
    public string $description = '';
    public ?int $id;
    public int $status;
    /**
     * @var OrderPadItemDTO[] $items
     */
    public array $items = [];
    public float $amount = 0.0;

    public function __construct(
        string $description,
        int $status,
        array $items,
        float $amount,
        ?int $id = null,
    ) {
        $this->description = $description;
        $this->status = $status;
        $this->items = $items;
        $this->amount = $amount;
        $this->id = $id;
    }
}
