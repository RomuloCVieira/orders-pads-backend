<?php

declare(strict_types=1);

namespace Api\Infrastructure\Http\Controllers;

use Api\Application\UseCaseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

interface ControllerInterface
{
    public function __construct(UseCaseInterface $useCaseInterface);
    public function __invoke(Request $request, Response $response, array $args): Response;
}
