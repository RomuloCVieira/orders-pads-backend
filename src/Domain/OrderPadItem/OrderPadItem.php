<?php

declare(strict_types=1);

namespace Api\Domain\OrderPadItem;

use Api\Domain\Build;
use Api\Domain\Product\Product;

class OrderPadItem
{
    use Build;

    public Product $product;
    public int $quantity;
    public float $amount = 0.0;
    public ?int $idtOrderPad;
    public ?int $idt;

    public function __construct(
        Product $product,
        int    $quantity,
        float  $amount,
        ?int   $idtOrderPad,
        ?int   $idt = null
    ) {
        $this->product = $product;
        $this->quantity = $quantity;
        $this->amount = $amount;
        $this->idtOrderPad = $idtOrderPad;
        $this->idt = $idt;
    }

    public function crediteItem(): void
    {
        $this->amount += $this->product->getAmount();
        $this->quantity += 1;
    }

    public function debitItem(): void
    {
        $this->amount -= $this->product->getAmount();
        $this->quantity -= 1;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}
