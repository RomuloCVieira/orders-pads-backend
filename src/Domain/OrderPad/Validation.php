<?php

declare(strict_types=1);

namespace Api\Domain\OrderPad;

use Api\Domain\Status\Closed;
use DomainException;

trait Validation
{
    private function validateDescription(string $descricao): void
    {
        $regex = "/^[A-ZÀ-Ú][a-zà-ú]* [A-ZÀ-Ú][A-zà-ú]*$/";
        if (!preg_match($regex, $descricao)) {
            throw new DomainException('Descrição invalida para OrderPad');
        }
    }

    private function isClosed(): void
    {
        if ($this->getStatus() instanceof Closed) {
            throw new DomainException('Não pode ser adionado item, comanda FECHADA');
        }
    }

    private function isEmptyItem(): void
    {
        if (empty($this->items)) {
            throw new DomainException('OrderPad não pode ser fechada sem itens');
        }
    }

    public function itemsIsNotEmpty(): bool
    {
        return !empty($this->items);
    }
}
