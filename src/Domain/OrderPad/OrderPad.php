<?php

declare(strict_types=1);

namespace Api\Domain\OrderPad;

use Api\Domain\Build;
use Api\Domain\OrderPadItem\OrderPadItem;
use Api\Domain\Product\Product;
use Api\Domain\Status\Canceled;
use Api\Domain\Status\Closed;
use Api\Domain\Status\Downloaded;
use Api\Domain\Status\Open;
use Api\Domain\Status\StatusInterface;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadItemDTO;
use DomainException;

class OrderPad
{
    use Validation;
    use Build;

    public const CREDIT = 'credit';
    public const DEBIT = 'debit';

    private StatusInterface $status;
    private ?int $idt;
    private float $amount;

    /**
     * @var OrderPadItem []
     */
    private array $items;
    private string $description;

    public function __construct(
        StatusInterface $status,
        int $idt = null,
        float $amount = 0.0,
        array $items = [],
        string $description = 'Default Description'
    ) {
        $this->validateDescription($description);

        $this->status = $status;
        $this->idt = $idt;
        $this->amount = $amount;
        $this->items = $items;
        $this->description = $description;

        $this->calculateTotalAmount();
    }

    public function calculateTotalAmount(): float
    {
        $this->amount = array_reduce(
            array: $this->getItems(),
            callback: function ($total, $item) {
                if ($this->itemsIsNotEmpty()) {
                    return $total += $item->amount;
                }
            },
            initial: 0.0
        );

        return $this->amount;
    }

    public function addItems(array $items): void
    {
        foreach ($items as $item) {
            $product = Product::build(['idt' => $item->idtproduct]);

            /**@var $item OrderPadItemDTO*/
            $this->addItem(new OrderPadItem(
                product: $product,
                quantity: $item->quantity,
                amount: ($product->getAmount() * $item->quantity),
                idtOrderPad: $item->idcomanda,
                idt: $item->idcomanda
            ));
        }
    }

    public function addItem(OrderPadItem $item): OrderPad
    {
        $this->isClosed();

        try {
            $this->findProduct($item);
        } catch (DomainException $exception) {
            array_push($this->items, $item);
        }

        $this->calculateTotalAmount();
        return $this;
    }

    public function creditOrDebitItems(array $orderPadItems, string $method): OrderPad
    {
        foreach ($orderPadItems as $item) {
            $filteredItem = $this->findProduct($item);

            if ($method === self::DEBIT) {
                $this->items[key($filteredItem)]->debitItem();
            }

            if ($method === self::CREDIT) {
                $this->items[key($filteredItem)]->crediteItem();
            }
        }

        $this->calculateTotalAmount();

        return $this;
    }

    private function findProduct(OrderPadItem $orderPadItem): array
    {
        $item = array_filter(
            $this->items,
            fn(OrderPadItem $item) => $item->getProduct()->getIdt() == $orderPadItem->getProduct()->getIdt()
        );

        if (empty($item)) {
            throw new DomainException('Nem um produto encontrado por favor adicionar produto!!!');
        }

        return $item;
    }

    public function updateStatus(int $status): void
    {
        switch ($status) {
            case Open::OPEN:
                $this->status = $this->status->openOrderPad();
                break;
            case Closed::CLOSED:
                $this->isEmptyItem();
                $this->status = $this->status->closeOrderPad();
                break;
            case Downloaded::DOWNLOADED:
                $this->status = $this->status->downloadOrderPad();
                break;
            case Canceled::CANCELED:
                $this->status = $this->status->cancelOrderPad();
                break;
        }
    }

    public function getStatus(): StatusInterface
    {
        return $this->status;
    }

    public function getIdt(): ?int
    {
        return $this->idt;
    }

    public function setIdt(int $idt): void
    {
        $this->idt = $idt;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
