<?php

declare(strict_types=1);

namespace Api\Domain\OrderPad;

use ArrayIterator;
use IteratorAggregate;

class OrderPadList implements IteratorAggregate
{
    /** @var OrderPad[] */
    private array $orderPadList;

    public function __construct()
    {
        $this->orderPadList = [];
    }

    public function addOrderPad(OrderPad $orderPad): void
    {
        $this->orderPadList[] = $orderPad;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->orderPadList);
    }
}
