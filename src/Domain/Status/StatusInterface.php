<?php

declare(strict_types=1);

namespace Api\Domain\Status;

interface StatusInterface
{
    public function openOrderPad(): Open;
    public function closeOrderPad(): Closed;
    public function downloadOrderPad(): Downloaded;
    public function cancelOrderPad(): Canceled;
    public function getId(): int;
}
