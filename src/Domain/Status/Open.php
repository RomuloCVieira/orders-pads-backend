<?php

declare(strict_types=1);

namespace Api\Domain\Status;

class Open extends Status
{
    const OPEN = 1;

    public function closeOrderPad(): Closed
    {
        return (new Closed());
    }

    public function cancelOrderPad(): Canceled
    {
        return (new Canceled());
    }

    public function getId(): int
    {
        return self::OPEN;
    }
}
