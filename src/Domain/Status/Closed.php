<?php

declare(strict_types=1);

namespace Api\Domain\Status;

class Closed extends Status
{
    const CLOSED = 2;

    public function downloadOrderPad(): Downloaded
    {
        return (new Downloaded());
    }

    public function openOrderPad(): Open
    {
        return (new Open());
    }

    public function getId(): int
    {
        return self::CLOSED;
    }
}
