<?php

declare(strict_types=1);

namespace Api\Domain\Status;

class Canceled extends Status
{
    const CANCELED = 3;

    public function getId(): int
    {
        return self::CANCELED;
    }
}
