<?php

declare(strict_types=1);

namespace Api\Domain\Status;

class Downloaded extends Status
{
    const DOWNLOADED = 4;

    public function getId(): int
    {
        return self::DOWNLOADED;
    }
}
