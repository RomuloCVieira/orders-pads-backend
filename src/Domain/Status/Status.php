<?php

declare(strict_types=1);

namespace Api\Domain\Status;

use DomainException;

abstract class Status implements StatusInterface
{
    public static function getStatus(int $status): StatusInterface|DomainException
    {
        return match ($status) {
            Open::OPEN => new Open(),
            Closed::CLOSED => new Closed(),
            Downloaded::DOWNLOADED => new Downloaded(),
            Canceled::CANCELED => new Canceled(),
            default => throw new DomainException('Status não encontrado'),
        };
    }

    public function openOrderPad(): Open
    {
        throw new DomainException(message: 'Não é possivel abrir comanda');
    }

    public function closeOrderPad(): Closed
    {
        throw new DomainException(message: 'Não é possivel fechar a comanda');
    }

    public function downloadOrderPad(): Downloaded
    {
        throw new DomainException(message: 'Não é possivel baixar a comanda');
    }

    public function cancelOrderPad(): Canceled
    {
        throw new DomainException(message: 'Não é possivel cancelar a comanda');
    }
}
