<?php

declare(strict_types=1);

namespace Api\Domain;

use ReflectionClass;
use ReflectionException;

trait Build
{
    /**
     * @throws ReflectionException
     */
    public static function build(array $args): self
    {
        $refection = new ReflectionClass(__CLASS__);

        return $refection->newInstanceArgs($args);
    }
}
