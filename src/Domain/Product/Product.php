<?php

declare(strict_types=1);

namespace Api\Domain\Product;

use Api\Domain\Build;

class Product
{
    use Build;

    private int $idt;
    private ?string $description;
    private ?float $amount;

    public function __construct(int $idt, ?string $description = 'Default', ?float $amount = 10)
    {
        $this->idt = $idt;
        $this->description = $description;
        $this->amount = $amount;
    }

    public function getIdt(): int
    {
        return $this->idt;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }
}
