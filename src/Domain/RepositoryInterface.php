<?php

declare(strict_types=1);

namespace Api\Domain;

use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPad\OrderPadList;

interface RepositoryInterface
{
    public function save(OrderPad $orderPad): OrderPad;
    public function update(OrderPad|array $orderPad): OrderPad|array;
    public function findAll(): OrderPadList;
    public function findById(int $idOrderPad): OrderPad|array;
}
