<?php

declare(strict_types=1);

namespace Api\Application;

use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPad\OrderPadList;
use Api\Domain\RepositoryInterface;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadDTO;

interface UseCaseInterface
{
    public function __construct(RepositoryInterface $repositoryInterface);
    public function action(OrderPadDTO $orderPadDTO): OrderPad|OrderPadList;
}
