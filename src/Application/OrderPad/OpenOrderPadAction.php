<?php

declare(strict_types=1);

namespace Api\Application\OrderPad;

use Api\Application\UseCaseInterface;
use Api\Domain\OrderPad\OrderPad;
use Api\Domain\RepositoryInterface;
use Api\Domain\Status\Open;
use Api\Domain\Status\Status;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadDTO;

class OpenOrderPadAction implements UseCaseInterface
{
    private RepositoryInterface $repositoryInterface;

    public function __construct(RepositoryInterface $repositoryInterface)
    {
        $this->repositoryInterface = $repositoryInterface;
    }

    public function action(OrderPadDTO $orderPadDTO): OrderPad
    {
        $orderPad = OrderPad::build([
                'description' => $orderPadDTO->description,
                'amount' => $orderPadDTO->amount,
                'status' => Status::getStatus(Open::OPEN),
        ]);

        $orderPad->addItems($orderPadDTO->items);

        return $this->repositoryInterface->save($orderPad);
    }
}
