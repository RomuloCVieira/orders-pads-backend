<?php

declare(strict_types=1);

namespace Api\Application\OrderPad;

use Api\Application\UseCaseInterface;
use Api\Domain\OrderPad\OrderPadList;
use Api\Domain\RepositoryInterface;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadDTO;
use Exception;

class FindOrderPadAction implements UseCaseInterface
{
    private RepositoryInterface $repositoryInterface;

    public function __construct(RepositoryInterface $repositoryInterface)
    {
        $this->repositoryInterface = $repositoryInterface;
    }

    public function action(OrderPadDTO $orderPadDTO): OrderPadList
    {
        $orderPads = $this->repositoryInterface->findAll();

        if (empty($orderPads)) {
            throw new Exception('Não foi encontrado comanda em aberto!!!');
        }

        return $orderPads;
    }
}
