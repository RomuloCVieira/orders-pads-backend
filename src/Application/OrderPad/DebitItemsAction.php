<?php

declare(strict_types=1);

namespace Api\Application\OrderPad;

use Api\Application\UseCaseInterface;
use Api\Domain\OrderPad\OrderPad;
use Api\Domain\OrderPad\OrderPadList;
use Api\Domain\OrderPadItem\OrderPadItem;
use Api\Domain\Product\Product;
use Api\Domain\RepositoryInterface;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadDTO;

class DebitItemsAction implements UseCaseInterface
{
    private RepositoryInterface $repositoryInterface;

    public function __construct(RepositoryInterface $repositoryInterface)
    {
        $this->repositoryInterface = $repositoryInterface;
    }

    public function action(OrderPadDTO $orderPadDTO): OrderPad|OrderPadList
    {
        $orderPadOld = $this->repositoryInterface->findById($orderPadDTO->id);

        $orderPadItem = array_map(function ($item) use ($orderPadDTO) {
            return OrderPadItem::build([
                'idtOrderPad' => $orderPadDTO->id,
                'product' => Product::build(['idt' => $item->idtproduct]),
                'quantity' => $item->quantity,
                'amount' => $item->amount
            ]);
        }, $orderPadDTO->items);

        $orderPad = $orderPadOld->creditOrDebitItems($orderPadItem, 'debit');

        return $this->repositoryInterface->update($orderPad);
    }
}
