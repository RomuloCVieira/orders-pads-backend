<?php

declare(strict_types=1);

namespace Api\Application\OrderPad;

use Api\Application\UseCaseInterface;
use Api\Domain\OrderPad\OrderPad;
use Api\Domain\RepositoryInterface;
use Api\Infrastructure\Http\Controllers\OrderPad\DTOS\OrderPadDTO;

class AddItemToOrderPadAction implements UseCaseInterface
{
    private RepositoryInterface $repositoryInterface;

    public function __construct(RepositoryInterface $repositoryInterface)
    {
        $this->repositoryInterface = $repositoryInterface;
    }

    public function action(OrderPadDTO $orderPadDTO): OrderPad
    {
        $orderPad = $this->repositoryInterface->findById($orderPadDTO->id);

        $orderPad->addItems($orderPadDTO->items);

        return $this->repositoryInterface->save($orderPad);
    }
}
